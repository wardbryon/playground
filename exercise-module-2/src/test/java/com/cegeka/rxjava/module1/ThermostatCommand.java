package com.cegeka.rxjava.module1;

public class ThermostatCommand {

    private double change;

    public ThermostatCommand(double change) {
        this.change = change;
    }

    public double getChange() {
        return change;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ThermostatCommand that = (ThermostatCommand) o;

        return Double.compare(that.change, change) == 0;

    }

    @Override
    public int hashCode() {
        long temp = Double.doubleToLongBits(change);
        return (int) (temp ^ (temp >>> 32));
    }

    @Override
    public String toString() {
        return "ThermostatCommand{" +
                "change=" + change +
                '}';
    }
}
