package com.cegeka.rxjava.module1;

import com.cegeka.rxjava.module1.helper.ThreadUtils;
import org.assertj.core.api.Assertions;
import org.junit.Test;
import rx.Observable;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

import java.util.concurrent.TimeUnit;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Your definitive guide to RxJava at
 * http://reactivex.io/documentation/operators.html
 */
public class ObservableCreationTest {

    /**
     * Example test-case, you don't have to change anything here.
     */
    @Test
    public void givenARangeOfIntegers_whenConcatenate_thenConcatenatedString(){
        Observable<Integer> range = Observable.range(11, 10);

        final StringBuilder concatenation = new StringBuilder();
        range.subscribe(integer -> concatenation.append(integer).append(" "));

        assertThat(concatenation.toString()).isEqualTo("11 12 13 14 15 16 17 18 19 20 ");
    }

    @Test
    public void givenARangeOfIntegers_whenConcatenate_thenConcatenatedStringContainsTripleOfAll(){

        final StringBuilder concatenation = new StringBuilder();

        // Your code here

        assertThat(concatenation.toString()).isEqualTo("33 36 39 42 45 48 51 54 57 60 63 ");
    }

    @Test
    public void givenAListOfStrings_whenConcatenate_thenConcatenatedStringContainsFirstLetterOfAllTheStrings(){

    }

    /**
     * Tips & Tricks
     * Search for Rx operator
     * Do not forget to give the thread some time
     */
    @Test
    public void givenASequenceOfIntegersSpacedByTimeInterval_whenConcatenate_thenConcatenatedStringContainsThemAll(){

        final StringBuilder concatenation = new StringBuilder();

        // Your code here

        assertThat(concatenation.toString()).isEqualTo("0 1 2 3 4 5 ");
    }



}
