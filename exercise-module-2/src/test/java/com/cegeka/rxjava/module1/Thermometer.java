package com.cegeka.rxjava.module1;

import rx.Observable;
import rx.subjects.BehaviorSubject;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class Thermometer implements Runnable{

    private final BehaviorSubject<Double> subject;
    private final List<Double> valuesToEmitAndRepeat;
    private final Integer interval;

    public Thermometer(Integer interval, Double... valuesToEmitAndRepeat){
        this.interval = interval;
        this.valuesToEmitAndRepeat = Arrays.asList(valuesToEmitAndRepeat);
        this.subject = BehaviorSubject.create();
    }

    public Observable<Double> asObservable(){
        new Thread(this).start();
        return subject;
    }

    @Override
    public void run() {
        int i = 0;
        while(true){
            sleep();
            subject.onNext(valuesToEmitAndRepeat.get(i++%valuesToEmitAndRepeat.size()));

        }

    }

    private void sleep() {
        try {
            TimeUnit.MILLISECONDS.sleep(interval);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }
}
