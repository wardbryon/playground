package com.cegeka.rxjava.module1.helper;

import java.util.concurrent.TimeUnit;

public class ThreadUtils {
    public static void sleepSeconds(int sleep, TimeUnit timeUnit) {
        try {
            timeUnit.sleep(sleep);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }

    public static void printCurrentThread(String info){
        System.out.println(info +
                " on " + Thread.currentThread().getName());
    }
}
