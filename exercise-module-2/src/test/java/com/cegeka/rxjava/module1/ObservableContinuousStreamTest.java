package com.cegeka.rxjava.module1;

import com.cegeka.rxjava.module1.helper.ThreadUtils;
import org.junit.Test;

import java.sql.Time;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.stream.DoubleStream;

import static org.assertj.core.api.Assertions.assertThat;

public class ObservableContinuousStreamTest {

    private Double average;

    @Test
    public void givenOutdoorThermometer_whenSampling_thenReturnsSample(){
        final List<Double> collection = new ArrayList<>();

        new Thermometer(100, 18.0, 19.0, 20.0, 21.0, 20.0, 19.0)
                .asObservable();
        //operate on the observable

        ThreadUtils.sleepSeconds(2300, TimeUnit.MILLISECONDS);

        assertThat(collection).hasSize(2);
    }

    @Test
    public void givenPreferredRoomTemperature_whenThermometerVaries_thenCreateThermostatCommands(){
        final List<ThermostatCommand> thermostatCommands = new ArrayList<>();
        double preferredRoomTemperature = 21.5;

        new Thermometer(100, 18.0, 19.0, 20.0, 21.0, 20.0, 19.0)
                .asObservable();
        //operate on the observable

        ThreadUtils.sleepSeconds(500, TimeUnit.MILLISECONDS);

        assertThat(thermostatCommands).containsExactly(
                            new ThermostatCommand(3.5),
                            new ThermostatCommand(2.5),
                            new ThermostatCommand(1.5),
                            new ThermostatCommand(0.5),
                            new ThermostatCommand(1.5),
                            new ThermostatCommand(2.5));
    }

    @Test
    public void givenPreferredRoomTemperature_whenThermometerVariesAndThermostatCommandChangeIsGreaterThan5_thenTakeAllTheCommandUntilThisOccurs(){
        final List<ThermostatCommand> thermostatCommands = new ArrayList<>();
        double preferredRoomTemperature = 21.5;

        new Thermometer(100, 18.0, 19.0, 20.0, 21.0, 16.0, 19.0)
                .asObservable();
        //operate on the observable

        ThreadUtils.sleepSeconds(500, TimeUnit.MILLISECONDS);

        assertThat(thermostatCommands).containsExactly(
                new ThermostatCommand(3.5),
                new ThermostatCommand(2.5),
                new ThermostatCommand(1.5),
                new ThermostatCommand(0.5));
    }

    @Test
    public void givenPreferredRoomTemperature_whenThermometerVaries_thenCreateAnAverageThermostatChangeCommandForVariationOf1Seconds() {
        final List<ThermostatCommand> thermostatCommands = new ArrayList<>();
        double preferredRoomTemperature = 21.5;

        new Thermometer(300, 18.0, 22.0, 20.0, 30.0, 30.0, 30.0)
                .asObservable();
        //operate on the observable

        ThreadUtils.sleepSeconds(2000, TimeUnit.MILLISECONDS);

        assertThat(thermostatCommands).containsExactly(
                new ThermostatCommand(1.5),
                new ThermostatCommand(-8.5)
                );
    }

}
