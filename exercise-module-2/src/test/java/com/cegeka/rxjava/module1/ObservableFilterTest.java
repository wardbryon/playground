package com.cegeka.rxjava.module1;

import com.cegeka.rxjava.module1.helper.Chronometer;
import com.cegeka.rxjava.module1.helper.ThreadUtils;
import org.assertj.core.util.Lists;
import org.junit.Test;
import rx.Observable;
import rx.functions.Func2;
import rx.schedulers.Schedulers;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Your definitive guide to RxJava at
 * http://reactivex.io/documentation/operators.html
 */
public class ObservableFilterTest {

    @Test
    public void givenARangeOfIntegersFrom1Until20_whenConcatenate_thenConcatenatedStringContainsTheEvenNumbers(){
        final StringBuilder concatenation = new StringBuilder();

        // Your code here

        assertThat(concatenation.toString()).isEqualTo("2 4 6 8 10 12 14 16 18 20 ");
    }

    @Test
    public void givenARangeOfIntegersFrom1Until20_whenConcatenate_thenConcatenatedStringContainsTheEvenNumbersInReverseOrder(){
        final List<Integer> sortedList = new ArrayList<>();

        // Your code here

        assertThat(sortedList).isEqualTo(Lists.newArrayList(20,18,16,14,12,10,8,6,4,2));
    }

    @Test
    public void givenARangeOfIntegersFrom1Until100_whenConcatenate_thenConcatenatedStringContainsOnlyThePrimeNumbers(){
        final StringBuilder concatenation = new StringBuilder();

        // Your code here

        assertThat(concatenation.toString()).isEqualTo("1 2 3 5 7 11 13 17 19 23 29 31 37 41 43 47 53 59 61 67 71 73 79 83 89 97 ");
    }



}
