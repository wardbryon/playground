package com.cegeka.rxjava.module1.helper;

public class Chronometer {
    private long start;
    private long stop;

    public void start(){
        start = System.currentTimeMillis();
    }
    public void stop(){
        stop = System.currentTimeMillis();
    }

    public long timeElapsed(){
        return stop - start;
    }
}
