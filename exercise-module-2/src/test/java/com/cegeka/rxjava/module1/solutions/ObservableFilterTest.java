package com.cegeka.rxjava.module1.solutions;

import com.cegeka.rxjava.module1.helper.Chronometer;
import com.cegeka.rxjava.module1.helper.ThreadUtils;
import org.assertj.core.util.Lists;
import org.junit.Test;
import rx.Observable;
import rx.schedulers.Schedulers;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Your definitive guide to RxJava at
 * http://reactivex.io/documentation/operators.html
 */
public class ObservableFilterTest {

    @Test
    public void givenARangeOfIntegersFrom1Until20_whenConcatenate_thenConcatenatedStringContainsTheEvenNumbers(){
        Observable<Integer> range = Observable.range(1, 20);
        final StringBuilder concatenation = new StringBuilder();

        // Your code here
        range
            .filter(integer -> integer%2 ==0)
            .subscribe(integer -> concatenation.append(integer).append(" "));

        assertThat(concatenation.toString()).isEqualTo("2 4 6 8 10 12 14 16 18 20 ");
    }

    @Test
    public void givenARangeOfIntegersFrom1Until20_whenConcatenate_thenConcatenatedStringContainsTheEvenNumbersInReverseOrder(){
        Observable<Integer> range = Observable.range(1, 20);
        final List<Integer> sortedList = new ArrayList<>();

        // Your code here
        range
                .filter(integer -> integer%2 ==0)
                .toSortedList((integer, integer2) -> integer2 - integer)
                .subscribe(list -> sortedList.addAll(list));

        assertThat(sortedList).isEqualTo(Lists.newArrayList(20,18,16,14,12,10,8,6,4,2));
    }

    @Test
    public void givenARangeOfIntegersFrom1Until100_whenConcatenate_thenConcatenatedStringContainsOnlyThePrimeNumbers(){
        Observable<Integer> range = Observable.range(1, 100);
        final StringBuilder concatenation = new StringBuilder();

        // Your code here
        range
            .filter(integer -> isPrime(integer))
            .subscribe(integer -> concatenation.append(integer).append(" "));

        assertThat(concatenation.toString()).isEqualTo("1 2 3 5 7 11 13 17 19 23 29 31 37 41 43 47 53 59 61 67 71 73 79 83 89 97 ");
    }

    /**
     * Tips & Tricks : parallel is removed from RxJava
     * So you will have to use one of the Schedulers
     *
     */
    @Test
    public void givenARangeOfIntegersFrom1Until100_whenPrimeCalculationTakes2SecondsButParallelized_thenConcatenatedStringContainsOnlyThePrimeNumbers() throws InterruptedException {
        Observable<Integer> range = Observable.range(1, 100);
        final StringBuilder concatenation = new StringBuilder();
        Chronometer chronometer = new Chronometer();
        chronometer.start();

        // Your code here
        range
                .subscribeOn(Schedulers.computation())
                .observeOn(Schedulers.computation())
                .filter(integer -> isPrime(integer))
                .subscribe(integer -> {concatenation.append(integer).append(" ");
                    ThreadUtils.printCurrentThread("subscribe");
                    chronometer.stop();});

        ThreadUtils.sleepSeconds(4, TimeUnit.SECONDS);

        assertThat(chronometer.timeElapsed()).isBetween(2000L, 3000L);
        assertThat(concatenation.toString()).isEqualTo("1 2 3 5 7 11 13 17 19 23 29 31 37 41 43 47 53 59 61 67 71 73 79 83 89 97 ");
    }

    private Boolean isPrime(Integer integer) {
        ThreadUtils.sleepSeconds(2, TimeUnit.SECONDS);
        ThreadUtils.printCurrentThread(""+integer);
        for(int i = 2; i < integer; i++){
            if(integer%i == 0){
                return false;
            }
        }
        return true;
    }


}
